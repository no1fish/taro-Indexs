import React, { Component } from 'react'
import { View, Text,ScrollView } from '@tarojs/components'
import './index.scss'
import Taro from "@tarojs/taro";
import { AtSearchBar,AtAvatar} from 'taro-ui'
import Request from "../../service/request";
import "taro-ui/dist/style/components/avatar.scss";
import {usersList} from './usersList'
import {connect} from "react-redux";
@connect(({ common}) => ({
  ...common,
}))
export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyList:[],
          // ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'],
      list :
         [
          {
        title: 'A',
        key: 'A',
        items: [
          {
            'name': '阿坝',
            'url':'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F10953168213%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1628837136&t=125ee4b33b3f3f5e2bbaa321cf44399d',
            'b':'beia',
            time:'2017年8月',
            active:false,
            id:1,
          },
          {
            'name': '阿坝',
            'url':'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F10953168213%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1628837136&t=125ee4b33b3f3f5e2bbaa321cf44399d',
            'b':'beia',
            time:'2017年8月',
            active:false,
            id:19,
          },
          {
            'name': '阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善',
            'url':'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F11950107544%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1628837136&t=2669a14e4702947222e8ff2cd3f13f7f',
             'b':'beiadasdasd',
            time:'2017年8月',
            active:false,
            id:2,
          }
          ]
      },

        {
          title: 'Q',
          key: 'Q',
          items: [
            {
              'name': '阿坝',
              'url':'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F10953168213%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1628837136&t=125ee4b33b3f3f5e2bbaa321cf44399d',
              'b':'beia',
              time:'2017年8月',
              active:false,
              id:15,
            },
            {
              'name': '阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善',
              'url':'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F11950107544%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1628837136&t=2669a14e4702947222e8ff2cd3f13f7f',
              'b':'beiadasdasd',
              time:'2017年8月',
              active:false,
              id:25,
            }
          ]
        },
         {
          title: 'B',
          key: 'B',
          items: [
            {
              'name': '北京',
              id:3,
              active:false,
            },
            {
              'name': '保定',
              id:4,
              active:false,
            }]
        },
          {
        title: 'C',
        key: 'C',
        items: [
          {
            'name': '阿坝',
            'url':'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F10953168213%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1628837136&t=125ee4b33b3f3f5e2bbaa321cf44399d',
            'b':'beia',
            time:'2017年8月',
            active:false,
            id:11,
          },
          {
            'name': '阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善',
            'url':'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F11950107544%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1628837136&t=2669a14e4702947222e8ff2cd3f13f7f',
             'b':'beiadasdasd',
            time:'2017年8月',
            active:false,
            id:21,
          }
          ]
      },
          {
        title: 'D',
        key: 'D',
        items: [
          {
            'name': '阿坝',
            'url':'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F10953168213%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1628837136&t=125ee4b33b3f3f5e2bbaa321cf44399d',
            'b':'beia',
            time:'2017年8月',
            active:false,
            id:13,
          },
          {
            'name': '阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善',
            'url':'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F11950107544%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1628837136&t=2669a14e4702947222e8ff2cd3f13f7f',
             'b':'beiadasdasd',
            time:'2017年8月',
            active:false,
            id:23,
          }
          ]
      },
          {
        title: 'E',
        key: 'E',
        items: [
          {
            'name': '阿坝',
            'url':'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F10953168213%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1628837136&t=125ee4b33b3f3f5e2bbaa321cf44399d',
            'b':'beia',
            time:'2017年8月',
            active:false,
            id:14,
          },
          {
            'name': '阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善',
            'url':'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F11950107544%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1628837136&t=2669a14e4702947222e8ff2cd3f13f7f',
             'b':'beiadasdasd',
            time:'2017年8月',
            active:false,
            id:24,
          }
          ]
      },
          {
        title: 'F',
        key: 'F',
        items: [
          {
            'name': '阿坝',
            'url':'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F10953168213%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1628837136&t=125ee4b33b3f3f5e2bbaa321cf44399d',
            'b':'beia',
            time:'2017年8月',
            active:false,
            id:15,
          },
          {
            'name': '阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善',
            'url':'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F11950107544%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1628837136&t=2669a14e4702947222e8ff2cd3f13f7f',
             'b':'beiadasdasd',
            time:'2017年8月',
            active:false,
            id:25,
          }
          ]
      },
          {
        title: 'G',
        key: 'G',
        items: [
          {
            'name': '阿坝',
            'url':'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F10953168213%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1628837136&t=125ee4b33b3f3f5e2bbaa321cf44399d',
            'b':'beia',
            time:'2017年8月',
            active:false,
            id:15,
          },
          {
            'name': '阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善阿拉善',
            'url':'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F11950107544%2F0.jpg&refer=http%3A%2F%2Finews.gtimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1628837136&t=2669a14e4702947222e8ff2cd3f13f7f',
             'b':'beiadasdasd',
            time:'2017年8月',
            active:false,
            id:25,
          }
          ]
      },
      ],
      value:'',
      scrollTop:0,
    };


  }
  componentWillMount () {
    console.log(1)
  }

  componentDidMount () {
    console.log(usersList)
    let data=usersList;
    console.log(data);
      let newList=data.message;
      let newKeyList=data.message.map(val=>{
        return val.key
      })
      for(let i in newList){
        if(newList[i].items.length===0){
          newList.splice(i,1);
        }
      }
      console.log(newList)
      // return;
      this.setState({
        list:newList.map(val=>{
          val.active=false;
          return val
        }),
        keyList:newKeyList
      })
    return
    /*接口請求數據*/
    Taro.showLoading({
      title: '加载中',
      mask:true,
    })
    Request({
      url:`/userBookList`,
      method: 'POST',
      data:{  },
    }).then(data=>{
      console.log(data);
      let newList=data.message;
      let newKeyList=data.message.map(val=>{
        return val.key
      })
      for(let i in newList){
        if(newList[i].items.length===0){
          newList.splice(i,1);
        }
      }
      console.log(newList)
      // return;
      this.setState({
        list:newList.map(val=>{
          val.active=false;
          return val
        }),
        keyList:newKeyList
      })
      Taro.hideLoading();
    })
  }

  componentWillUnmount () { }

  componentDidShow () {
    console.log(3)

  }

  componentDidHide () { }
  onChange (value) {
    this.setState({
      value: value
    })
    console.log(value)
  }
  onActionClick () {
    /*返回上一页*/
    Taro.navigateBack({
      delta: 1
    })
  }
  atcClick(item){
    console.log(item);

    let newlist=JSON.parse(JSON.stringify(this.state.list));
    for(let v of newlist){
      for(let j of v.items){
        if(j.id==item){
          j.active=!j.active;
        }
      }
    }
    let newActionAll=[];
    for(let v of newlist){
      for(let j of v.items){
        if(j.active){
          newActionAll.push({
            toUserName:j.name,
            toUserId:j.id
          })
        }
      }
    }
    this.changeState({
      ActionAll:newActionAll
    })
    console.log(newActionAll)
    this.setState({
      list: newlist
    })
  }
  goToIndex(val){
    /*const query = Taro.createSelectorQuery()
    query.select('.topSea').boundingClientRect( rec => {
      console.log(rec)
    }).exec(res=>{
      console.log(res);
    })*/
    /**
     * h1 搜索框高度
     * h2 A,B..的高度
     * h3 单个信息的高度
     * **/
    let h1=0;
    let h2=0;
    let h3=0;
    const query = Taro.createSelectorQuery().select('#topSea').boundingClientRect().selectViewport().scrollOffset().exec(function(res){
      console.log(res);
      h1=res[0].height;
      console.log(h1)
    });
    const queryletter = Taro.createSelectorQuery().select('.letter').boundingClientRect().selectViewport().scrollOffset().exec(function(res){
      console.log(res);
      h2=res[0].height
    });
    const queryboxSec = Taro.createSelectorQuery().select('.boxSec').boundingClientRect().selectViewport().scrollOffset().exec(function(res){
      console.log(res,'h33333');
      h3=res[0].height
    });
    let list=this.state.list;
    let number=0;
    let child=0;
    for(let v=0; v<list.length;v++){
      if(list[v].key==val){
        break;
      }
      number++;
      child+=list[v].items.length


    }
    setTimeout(()=>{
      console.log(number,child,h1);
      this.setState({
        scrollTop: h1+number*h2+child*h3
        // scrollTop: 42+number*(66)+child*88
      })
      console.log(this.state.scrollTop)
    },200)

  }
  onScrollToUpper() {}

  // or 使用箭头函数
  // onScrollToUpper = () => {}

  onScroll(e){
    console.log(e.detail)
  }
  Top(){
    console.log(12)
    this.setState({
      scrollTop:0.1
    })
    console.log(this.state.scrollTop)
  }
  changeState= (obj)=>{
    this.props.dispatch({
      type: 'common/changeKeyValue',
      payload: obj
    })
  };
  render () {
    const scrollStyle = {
      height: '100vh'
    }
    const Threshold = 20
    return (
        <ScrollView
            className='scrollview'
            scrollY
            scrollWithAnimation
            scrollTop={this.state.scrollTop}
            style={scrollStyle}
            lowerThreshold={Threshold}
            upperThreshold={Threshold}
            onScrollToUpper={this.onScrollToUpper.bind(this)}
            onScroll={this.onScroll}
        >
          <View >
            <View id='topSea'>
              {/*查看选中的数据+返回上一页*/}
              {/* <View className='topName'>
                <Text>
                  {
                    this.props.ActionAll?this.props.ActionAll.map((val,key)=>{
                      return <Text key={key} className='toUserName'> @ {val.toUserName}  </Text>
                    }):null
                  }
                </Text>
                <Text onClick={this.onActionClick.bind(this)} className='over'>完成</Text>
              </View> */}
            </View>
            <View>
              {
                this.state.list.map((item,key)=>{
                  return <View key={key}>
                    <View className='letter'>{item.key}</View>
                    <View className='box'>
                      {
                        item.items.map((val,k)=>{
                          return <View key={k} className='boxSec' onClick={this.atcClick.bind(this,val.id)}>
                            <View className='oc'>
                              {
                                val.active?<View className='oco'></View>:''
                              }
                            </View>
                            <View className='userImage'>
                              <AtAvatar image={val.wxImage} circle size='normal'></AtAvatar>
                            </View>
                            <View className='someSec'>
                              <View className='sec'> {val.name}</View>
                              <View className='sec sec0'> {val.department}</View>
                              <View className='sec sec0'>添加时间：{val.cdate}</View>
                            </View>
                          </View>
                        })
                      }
                    </View>
                  </View>
                })
              }
            </View>
            <View className='indexesList'>
              <View onClick={this.Top.bind(this)}>Top</View>
              {
                this.state.keyList.map((val,key)=>{
                  return <View key={key} onClick={this.goToIndex.bind(this,val)}>
                    <View>{val}</View>
                  </View>
                })
              }
            </View>
          </View>
        </ScrollView>

    )
  }
}
